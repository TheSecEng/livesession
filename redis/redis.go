package redis

import (
	"errors"
	"math/rand"
	"time"

	"github.com/apex/log"
	"github.com/gomodule/redigo/redis"
	"gitlab.com/theseceng/livesession/node"
)

const (
	maxReconnectAttempts     = 5
	defaultKeepaliveInterval = 30
)

// Config contains Redis pubsub adapter configuration
type Config struct {
	// Redis instance URL or master name in case of sentinels usage
	URL string
	// Redis Password
	Password string
	// Redis channel to subscribe to
	Channel string
	// Redis keepalive ping interval (seconds)
	KeepalivePingInterval int
}

// NewConfig builds a new config for Redis pubsub
func NewConfig() Config {
	return Config{KeepalivePingInterval: defaultKeepaliveInterval}
}

// Engine contains information about Redis pubsub connection
type Engine struct {
	node             node.AppNode
	url              string
	password         string
	pingInterval     time.Duration
	channel          string
	reconnectAttempt int
	log              *log.Entry
}

// NewEngine returns new RedisEngine struct
func NewEngine(node node.AppNode, config *Config) *Engine {
	return &Engine{
		node:             node,
		url:              config.URL,
		password:         config.Password,
		channel:          config.Channel,
		pingInterval:     time.Duration(config.KeepalivePingInterval),
		reconnectAttempt: 0,
		log:              log.WithFields(log.Fields{"context": "pubsub"}),
	}
}

// Start connects to Redis and subscribes to the pubsub channel
func (r *Engine) Start() error {
	for {
		if err := r.listen(); err != nil {
			r.log.Warnf("Redis connection failed: %v", err)
		}

		r.reconnectAttempt++

		if r.reconnectAttempt >= maxReconnectAttempts {
			return errors.New("redis reconnect attempts exceeded")
		}

		delay := attemptRetry(r.reconnectAttempt)

		r.log.Infof("Next Redis reconnect attempt in %s", delay)
		time.Sleep(delay)

		r.log.Infof("Reconnecting to Redis...")
	}
}

func (r *Engine) listen() error {

	c, err := redis.DialURL(r.url, redis.DialPassword(r.password))

	if err != nil {
		return err
	}

	defer c.Close()

	psc := redis.PubSubConn{Conn: c}
	if err := psc.Subscribe(r.channel); err != nil {
		r.log.Errorf("Failed to subscribe to Redis channel: %v", err)
		return err
	}

	r.reconnectAttempt = 0

	done := make(chan error, 1)

	go func() {
		for {
			switch v := psc.Receive().(type) {
			case redis.Message:
				r.log.Debugf("Incoming pubsub message from Redis: %s", v.Data)
				r.node.HandlePubSub(v.Data)
			case redis.Subscription:
				r.log.Infof("Subscribed to Redis channel: %s\n", v.Channel)
			case error:
				r.log.Errorf("Redis subscription error: %v", v)
				done <- v
			}
		}
	}()

	ticker := time.NewTicker(r.pingInterval * time.Second)
	defer ticker.Stop()

loop:
	for err == nil {
		select {
		case <-ticker.C:
			if err = psc.Ping(""); err != nil {
				break loop
			}
		case err := <-done:
			// Return error from the receive goroutine.
			return err
		}
	}

	psc.Unsubscribe()
	return <-done
}

func attemptRetry(step int) time.Duration {
	secs := (step * step) + (rand.Intn(step*4) * (step + 1))
	return time.Duration(secs) * time.Second
}
