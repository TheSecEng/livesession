/*
Package cmd is the cli interface
*/
/*
Copyright © 2020 Terminal

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package cmd

import (
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"syscall"

	"github.com/apex/log"
	"github.com/spf13/cobra"
	"github.com/syossan27/tebata"
	"gitlab.com/theseceng/livesession/config"
	"gitlab.com/theseceng/livesession/metrics"
	"gitlab.com/theseceng/livesession/node"
	"gitlab.com/theseceng/livesession/redis"
	"gitlab.com/theseceng/livesession/server"
	"gitlab.com/theseceng/livesession/util"
)

var CertPath string
var KeyPath string
var RedisPassword string
var Verbosity string

var serveCmd = &cobra.Command{
	Use:   "serve",
	Short: "Start the Live Session Server",
	Run: func(_ *cobra.Command, _ []string) {
		config := config.New()
		config.Host = "localhost"
		config.Port = 8080
		config.Path = "/ws"
		config.HealthPath = "/health"
		config.Redis.URL = "redis://redis:6379"
		config.Redis.Channel = "_livesession_"
		config.Redis.KeepalivePingInterval = 30
		config.BroadcastAdapter = "redis"
		config.WS.ReadBufferSize = 65535
		config.WS.WriteBufferSize = 65535
		config.WS.MaxMessageSize = 4000000 // 1 MB
		config.WS.EnableCompression = false
		config.Metrics.Log = true
		config.Metrics.LogInterval = 15 // Seconds
		config.Metrics.LogFormatter = ""
		config.LogLevel = Verbosity
		config.LogFormat = "text"

		ssl := server.NewSSLConfig()
		ssl.CertPath = CertPath
		ssl.KeyPath = KeyPath
		server.SSL = &ssl

		err := util.InitLogger(config.LogFormat, config.LogLevel)
		if err != nil {
			log.Errorf("!!! Failed to initialize logger !!!\n%v", err)
			os.Exit(1)
		}
		ctx := log.WithFields(log.Fields{"context": "main"})
		ctx.Infof("Starting LiveSession %s (pid: %d, open file limit: %s)", util.Version(), os.Getpid(), util.OpenFileLimit())

		metrics, err := metrics.FromConfig(&config.Metrics)
		if err != nil {
			log.Errorf("!!! Failed to initialize custom log printer !!!\n%v", err)
			os.Exit(1)
		}
		appNode := node.NewNode(metrics)
		appNode.Start()
		subscriber := redis.NewEngine(appNode, &config.Redis)
		go func() {
			if err := subscriber.Start(); err != nil {
				ctx.Errorf("!!! Subscriber failed !!!\n%v", err)
				os.Exit(1)
			}
		}()

		wsServer, err := server.ForPort(strconv.Itoa(config.Port))
		if err != nil {
			fmt.Printf("!!! Failed to initialize WebSocket server at %s:%s !!!\n%v", err, config.Host, config.Port)
			os.Exit(1)
		}
		wsServer.Mux.Handle(config.Path, node.WebsocketHandler(appNode, config.Headers, &config.WS))

		ctx.Infof("Handle WebSocket connections at %s%s", wsServer.Address(), config.Path)

		wsServer.Mux.Handle(config.HealthPath, http.HandlerFunc(server.HealthHandler))
		ctx.Infof("Handle health connections at %s%s", wsServer.Address(), config.HealthPath)

		go func() {
			if err = wsServer.StartAndAnnounce("WebSocket server"); err != nil {
				if !wsServer.Stopped() {
					log.Errorf("WebSocket server at %s stopped: %v", wsServer.Address(), err)
					os.Exit(1)
				}
			}
		}()

		go func() {
			if err := metrics.Run(); err != nil {
				ctx.Errorf("!!! Metrics module failed to start !!!\n%v", err)
				os.Exit(1)
			}
		}()

		t := tebata.New(syscall.SIGINT, syscall.SIGTERM)

		t.Reserve(func() {
			ctx.Infof("Shutting down... (hit Ctrl-C to stop immediately)")
			go func() {
				termSig := make(chan os.Signal, 1)
				signal.Notify(termSig, syscall.SIGINT, syscall.SIGTERM)
				<-termSig
				ctx.Warnf("Immediate termination requested. Stopped")
				os.Exit(0)
			}()
		})
		t.Reserve(metrics.Shutdown)
		t.Reserve(wsServer.Stop)
		t.Reserve(appNode.Shutdown)

		t.Reserve(os.Exit, 0)

		// Hang forever unless Exit is called
		select {}
	},
}

func init() {
	serveCmd.Flags().StringVarP(&CertPath, "cert", "c", "", "SSL certificate file path")
	serveCmd.Flags().StringVarP(&KeyPath, "key", "k", "", "SSL key file path")
	serveCmd.Flags().StringVarP(&RedisPassword, "password", "p", "", "Password for redis database")
	serveCmd.Flags().StringVarP(&Verbosity, "verbosity", "v", "info", "Logging verbosity")
	rootCmd.AddCommand(serveCmd)
}
