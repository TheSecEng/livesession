clean:
	rm -rf ./dist/linux/lss
	rm -rf ./dist/macos/lss
	rm -rf ./dist/win/lss

build-linux:
	GOOS=linux GOARCH=386 go build -o ./dist/linux/lss main.go

build-mac:
	go build -o ./dist/macos/lss main.go

to-trmnl:
	scp ./dist/linux/lss root@trmnl.io:./lss