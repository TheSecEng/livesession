module gitlab.com/theseceng/livesession

go 1.14

require (
	github.com/anycable/anycable-go v1.0.1
	github.com/apex/log v1.6.0
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/gorilla/websocket v1.4.2
	github.com/json-iterator/go v1.1.10
	github.com/matoous/go-nanoid v1.4.1
	github.com/mattn/go-isatty v0.0.12
	github.com/mitchellh/go-mruby v0.0.0-20200315023956-207cedc21542
	github.com/satori/go.uuid v1.2.0
	github.com/segmentio/fasthash v1.0.3
	github.com/sirupsen/logrus v1.2.0
	github.com/spf13/cobra v1.0.0
	github.com/syossan27/tebata v0.0.0-20180602121909-b283fe4bc5ba
	golang.org/x/crypto v0.0.0-20200728195943-123391ffb6de // indirect
)
