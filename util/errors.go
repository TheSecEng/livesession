package util

import (
	"encoding/json"
	"errors"
)

var (
	ErrNonExistentClient            = errors.New("nonexistent client")
	ErrNonExistentSession           = errors.New("nonexistent session")
	ErrDuplicateClientID            = errors.New("duplicate client id")
	ErrDuplicateSessionID           = errors.New("duplicate session id")
	ErrInvalidClientID              = errors.New("invalid client id")
	ErrInvalidSessionID             = errors.New("invalid session id")
	ErrInvalidSessionPassphrase     = errors.New("invalid session passphrase")
	ErrFailedAddClient              = errors.New("failed to add client")
	ErrFailedAddSession             = errors.New("failed to add session")
	ErrUnregisteringClient          = errors.New("failed to unregister client")
	ErrClientInSession              = errors.New("client already in active session")
	ErrClientUnexpectedDisconnect   = errors.New("client unexpectedly disconnected")
	ErrInvalidHostPermisssions      = errors.New("client is not the host")
	ErrInvalidEditorPermisssions    = errors.New("client is not an editor")
	ErrBadPayload                   = errors.New("corrupt payload")
	ErrJoinEncryptedMissingParam    = errors.New("attempted to join encrypted session without passing the encrypted parameter")
	ErrWebsocketClientInitialFailed = errors.New("websocket client initialization failed")
	ErrSessionBlocked               = errors.New("blocked from session")
)

var (
	SuccessAddedClient        = "client added successfully"
	SuccessAddedSession       = "session added successfully"
	SuccessLeftSession        = "client left session successfully"
	SuccessUnregisteredClient = "client unregistered successfully"
	SuccessRegisteredClient   = "client unregistered successfully"
)

// IsInvalidUnmarshalError returns boolean indicating whether the error is a *InvalidUnmarshalError
func IsInvalidUnmarshalError(err error) bool {
	if _, ok := err.(*json.InvalidUnmarshalError); ok {
		return true
	}
	return false
}
