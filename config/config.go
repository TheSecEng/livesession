package config

import (
	"gitlab.com/theseceng/livesession/metrics"
	"gitlab.com/theseceng/livesession/node"
	"gitlab.com/theseceng/livesession/redis"
)

// Config contains main application configuration
type Config struct {
	Redis            redis.Config
	Host             string
	Port             int
	BroadcastAdapter string
	Path             string
	HealthPath       string
	Headers          []string
	// SSL                  server.SSLConfig
	WS             node.WSConfig
	MaxMessageSize int64
	LogLevel       string
	LogFormat      string
	Metrics        metrics.Config
}

// New returns a new empty config
func New() Config {
	config := Config{}
	// config.SSL = server.NewSSLConfig()
	config.WS = node.NewWSConfig()
	config.Metrics = metrics.NewConfig()
	config.Redis = redis.NewConfig()
	return config
}
