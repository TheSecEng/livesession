package node

// EventTypes
var (
	// Generic Types for Message Statuses
	// UNKNOWN - Generally unused
	// SUCCESS - Anytime a user action is successful
	// FAILED - Anytime a user action fails
	TypeUnknown           = 1
	TypeSuccess           = 2
	TypeFailed            = 3
	TypeCreateSuccess     = 4
	TypeCreateFailed      = 5
	TypeJoinSuccess       = 6
	TypeJoinFailed        = 7
	TypeRequestFullBuffer = 8
	TypeRequestWrite      = 9
	TypeApproveWrite      = 10
	TypeDenyWrite         = 11
	TypeRevokeWrite       = 12
	TypeFileOpened        = 13
	TypeFileClosed        = 14
	TypeFileFocused       = 15
	TypeRequestWorkspace  = 16
	TypeWorkspace         = 17
	TypeFullBuffer        = 18
	TypeIncremental       = 19
	TypeSelectionModified = 20
	TypeBlockClient       = 21
	TypeUnblockClient     = 22
	TypeGetClients        = 23
	TypeFocusSession      = 24
	TypeFocusView         = 25
	TypeEnforceFollow     = 26
	TypeUpdateClientInfo  = 27
	TypeClientInfo        = 28
	TypeClientLeft        = 29
	TypeExit              = 30
	TypePing              = 31
)
