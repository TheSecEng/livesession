package node

import (
	"fmt"
	"net/http"
	"regexp"
	"strconv"
	"time"

	"github.com/apex/log"
	"github.com/gorilla/websocket"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/theseceng/livesession/util"
)

var (
	displayNameRE = regexp.MustCompile(`[^a-zA-Z0-9\_\-\.]+`)
)

// WSConfig contains WebSocket connection configuration.
type WSConfig struct {
	ReadBufferSize    int
	WriteBufferSize   int
	MaxMessageSize    int64
	EnableCompression bool
}

// NewWSConfig build a new WSConfig struct
func NewWSConfig() WSConfig {
	return WSConfig{}
}

// WebsocketHandler generate a new http handler for WebSocket connections
func WebsocketHandler(app *Node, fetchHeaders []string, config *WSConfig) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := log.WithField("context", "ws")

		if _, found := r.URL.Query()["action"]; !found {
			ctx.Info("no action specified")
			return
		}

		upgrader := websocket.Upgrader{
			CheckOrigin:       func(r *http.Request) bool { return true },
			ReadBufferSize:    config.ReadBufferSize,
			WriteBufferSize:   config.WriteBufferSize,
			EnableCompression: config.EnableCompression,
		}

		ws, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			ctx.Debugf("Websocket connection upgrade error: %#v", err.Error())
			return
		}

		url := r.URL.String()

		if !r.URL.IsAbs() {
			scheme := "http://"
			if r.TLS != nil {
				scheme = "https://"
			}
			url = fmt.Sprintf("%s%s%s", scheme, r.Host, url)
		}

		headers := util.FetchHeaders(r, fetchHeaders)
		ws.SetReadLimit(config.MaxMessageSize)

		if config.EnableCompression {
			ws.EnableWriteCompression(true)
		}

		switch action := r.URL.Query()["action"]; action[0] {
		case "create":
			HandleCreate(app, ws, r, url, headers, ctx)
		case "join":
			HandleJoin(app, ws, r, url, headers, ctx)
		default:
			return
		}
	})
}

func HandleCreate(app *Node, ws *websocket.Conn, r *http.Request, url string, headers map[string]string, ctx *log.Entry) {
	var displayName []string
	var encrypted []string
	var encrypt bool
	var err error

	encrypt = false

	displayName, ok := r.URL.Query()["display_name"]
	if !ok {
		displayName = []string{"No DisplayName"}
	}
	displayName[0] = displayNameRE.ReplaceAllString(displayName[0], "")

	encrypted, ok = r.URL.Query()["encrypted"]
	if !ok {
		encrypt = false
	} else {
		encrypt, err = strconv.ParseBool(encrypted[0])
		if err != nil {
			encrypt = false
		}
	}
	session := NewSession()
	session.Encrypted = encrypt

	app.hub.AddSession(session)

	go func() {
		client, err := NewHost(app, ws, url, headers, session.UID, displayName[0])
		if err != nil {
			ctx.Errorf("websocket client initialization failed: %v", err)
			util.CloseWS(ws, CloseAbnormalClosure, ErrWebsocketClientInitialFailed.Error())
			return
		}
		payload := &Payload{
			UID:     uuid.NewV4().String(),
			Type:    TypeCreateSuccess,
			Session: session,
			Sender:  client,
		}
		client.log.Debug("websocket client established")
		err = client.write(*payload, time.Now().Add(writeWait))
		if err != nil {
			ctx.Errorf("websocket client initialization failed: %v", err)
			util.CloseWS(ws, CloseAbnormalClosure, ErrWebsocketClientInitialFailed.Error())
			return
		}
		client.Start()
		client.log.Debug("websocket client completed")
	}()
}

func HandleJoin(app *Node, ws *websocket.Conn, r *http.Request, url string, headers map[string]string, ctx *log.Entry) {
	var requestedSession []string
	var displayName []string
	var encrypted []string
	var encrypt bool
	var err error

	requestedSession, ok := r.URL.Query()["session"]
	if !ok {
		ctx.Error("no session requested")
		util.CloseWS(ws, CloseAbnormalClosure, ErrNonExistentSession.Error())
		return
	}

	passphrase, ok := r.URL.Query()["passphrase"]
	if !ok {
		ctx.Error("no session passphrase detected")
		util.CloseWS(ws, CloseAbnormalClosure, ErrInvalidSessionPassphrase.Error())
		return
	}

	displayName, ok = r.URL.Query()["display_name"]
	if !ok {
		displayName = []string{"No DisplayName"}
	}

	displayName[0] = displayNameRE.ReplaceAllString(displayName[0], "")

	encrypted, ok = r.URL.Query()["encrypted"]
	if !ok {
		encrypt = false
	} else {
		encrypt, err = strconv.ParseBool(encrypted[0])
		if err != nil {
			encrypt = false
		}
	}

	session, ok := app.hub.sessions[requestedSession[0]]
	if !ok {
		ctx.Error("invalid session")
		util.CloseWS(ws, CloseAbnormalClosure, ErrInvalidSessionID.Error())
		return
	}

	remoteAddr := util.Hash(ws.RemoteAddr().String())
	if _, ok := session.blockedClients[remoteAddr]; ok {
		util.CloseWS(ws, CloseAbnormalClosure, ErrSessionBlocked.Error())
	}

	if session.Encrypted && !encrypt {
		ctx.Error(ErrJoinEncryptedMissingParam.Error())
		util.CloseWS(ws, CloseAbnormalClosure, ErrJoinEncryptedMissingParam.Error())
		return
	}

	go func() {
		client, err := NewClient(app, ws, url, headers, session.UID, passphrase[0], displayName[0])
		if err != nil {
			ctx.Errorf("websocket client initialization failed: %v", err)
			util.CloseWS(ws, CloseAbnormalClosure, ErrWebsocketClientInitialFailed.Error())
			return
		}
		payload := &Payload{
			UID:     uuid.NewV4().String(),
			Type:    TypeJoinSuccess,
			Session: session,
			Sender:  client,
		}
		fmt.Printf("ClientID: %s", client.UID)
		client.log.Debug("websocket client established")
		err = client.write(*payload, time.Now().Add(writeWait))
		if err != nil {
			ctx.Errorf("websocket client initialization failed: %v", err)
			util.CloseWS(ws, CloseAbnormalClosure, ErrWebsocketClientInitialFailed.Error())
			return
		}
		app.hub.BroadcastClientInfo(session, client)
		client.Start()
		client.log.Debug("websocket client completed")
	}()
}
