package node

import (
	"sync"

	"github.com/apex/log"
	"gitlab.com/theseceng/livesession/util"
)

// Hub stores all the sessions and the corresponding subscriptions info
type Hub struct {
	sessions  map[string]*Session
	clients   map[string]string
	broadcast chan *Payload
	shutdown  chan struct{}
	done      sync.WaitGroup
	sync.RWMutex
	log *log.Entry
}

// NewHub builds new hub instance
func NewHub() *Hub {
	return &Hub{
		sessions:  make(map[string]*Session),
		broadcast: make(chan *Payload, 256),
		shutdown:  make(chan struct{}),
		log:       log.WithFields(log.Fields{"context": "hub"}),
	}
}

// Run makes hub active
func (h *Hub) Run() {
	h.done.Add(1)
	for {
		select {
		case message := <-h.broadcast:
			h.ProcessPayload(message)
		case <-h.shutdown:
			h.done.Done()
			return
		}
	}
}

func (h *Hub) ProcessPayload(payload *Payload) {
	var session *Session
	var sender *Client
	var ok bool

	ctx := h.log.WithField("session", payload.Session.UID)
	ctx.Debugf("Broadcast message: payload %s with type %d in session %s", payload.UID, payload.Type, payload.Session.UID)
	h.RLock()
	if session, ok = h.sessions[payload.Session.UID]; !ok {
		h.RUnlock()
		ctx.Debug("No session found")
		return
	}
	h.RUnlock()

	session.RLock()
	if payload.Sender.UID == session.Host.UID {
		sender = session.Host
	} else if sender, ok = session.Clients[payload.Sender.UID]; !ok {
		session.RUnlock()
		ctx.Debug("No sender found")
		return
	}
	session.RUnlock()

	err := session.Validate(payload)
	if err != nil {
		ctx.Debug(err.Error())
		return
	}
	switch payload.Type {
	case TypeExit:
		h.handleHostExit(session, payload)
	case TypeBlockClient:
		h.handleBlockClient(session, payload)
	case TypeClientLeft:
		h.handleClientLeft(session, sender, payload)
	case TypeClientInfo:
		h.handleClientInfo(session, sender, payload)
	case TypeRequestFullBuffer, TypeRequestWrite, TypeRequestFullBuffer:
		h.broadcastToHost(payload)
	case TypeApproveWrite:
		h.handleApproveWrite(session, payload)
	case TypeDenyWrite:
		h.handleDenyWrite(session, payload)
	default:
		h.broadcastToSession(payload)
	}
}

// Shutdown sends shutdown command to hub
func (h *Hub) Shutdown() {
	h.shutdown <- struct{}{}

	// Wait for stop listening channels
	h.done.Wait()
}

func (h *Hub) AddSession(s *Session) {
	// fmt.Println("locking Hub")
	h.Lock()
	h.sessions[s.UID] = s
	h.Unlock()
	// fmt.Println("Unlocking Hub")
}

func (h *Hub) RemoveSession(uid string) {
	h.Lock()
	delete(h.sessions, uid)
	h.Unlock()
}

// Size returns a number of active clients
func (h *Hub) Size() int {
	h.RLock()
	count := 0
	for _, session := range h.sessions {
		count += len(session.Clients) + 1
	}
	h.RUnlock()
	return count
}

// SessionSize returns a number of uniq streams
func (h *Hub) SessionSize() int {
	return len(h.sessions)
}

func (h *Hub) BroadcastClientInfo(s *Session, c *Client) {
	ctx := h.log.WithField("broadcast_client_info", s.UID)
	rawClient, err := json.Marshal(c)
	if err != nil {
		ctx.Debug(err.Error())
		return
	}
	payload := NewPayload(TypeClientInfo)
	payload.Session = s
	payload.Sender = c
	payload.Body = rawClient
	h.broadcast <- payload
}

func (h *Hub) broadcastToHost(p *Payload) {
	ctx := h.log.WithField("session", p.Session.UID)
	ctx.Debugf("Broadcast to host message: payload %s session %s", p.UID, p.Session.UID)

	if s, ok := h.sessions[p.Session.UID]; !ok {
		ctx.Debug("No session found")
		return
	} else {
		s.Host.Send(p)
	}
}

func (h *Hub) broadcastToSession(p *Payload) {
	ctx := h.log.WithField("session", p.Session.UID)
	ctx.Debugf("Broadcast message: payload %s session %s", p.UID, p.Session.UID)

	h.RLock()
	if _, ok := h.sessions[p.Session.UID]; !ok {
		ctx.Debug("No session found")
		h.RUnlock()
		return
	}
	h.RUnlock()

	h.RLock()
	session := h.sessions[p.Session.UID]
	session.RLock()
	for _, client := range session.Clients {
		client.Send(p)
	}
	session.RUnlock()
	h.RUnlock()
}

func (h *Hub) handleHostExit(s *Session, p *Payload) {
	h.broadcastToSession(p)
	h.RemoveSession(s.UID)
}

func (h *Hub) handleBlockClient(s *Session, p *Payload) {
	receivedClient := &Client{}
	err := json.Unmarshal(p.Body, receivedClient)
	if err != nil {
		h.log.Info(err.Error())
		return
	}

	s.RLock()
	c, ok := s.Clients[receivedClient.UID]
	if !ok {
		s.RUnlock()
		return
	}
	s.RUnlock()

	remoteAddr := util.Hash(c.ws.RemoteAddr().String())
	s.Lock()
	s.blockedClients[remoteAddr] = true
	s.Unlock()

	s.RemoveClient(c)

	payload := &Payload{
		Type:    TypeClientLeft,
		Session: s,
		Sender:  s.Host,
		Body:    p.Body,
	}

	s.Host.Send(payload)
	h.broadcastToSession(payload)
	c.Close()
	c = nil
}

func (h *Hub) handleApproveWrite(s *Session, p *Payload) {
	receivedClient := &Client{}
	err := json.Unmarshal(p.Body, receivedClient)
	if err != nil {
		h.log.Info(err.Error())
		return
	}

	s.RLock()
	client, ok := s.Clients[receivedClient.UID]
	if !ok {
		s.RUnlock()
		return
	}
	s.RUnlock()

	client.mu.Lock()
	client.IsEditor = true
	client.mu.Unlock()

	client.Send(p)
}

func (h *Hub) handleDenyWrite(s *Session, p *Payload) {
	receivedClient := &Client{}
	err := json.Unmarshal(p.Body, receivedClient)
	if err != nil {
		h.log.Info(err.Error())
		return
	}

	s.RLock()
	client, ok := s.Clients[receivedClient.UID]
	if !ok {
		return
	}
	s.RUnlock()

	client.mu.Lock()
	client.IsEditor = false
	client.mu.Unlock()

	client.Send(p)
}

func (h *Hub) handleRevokeWrite(session *Session, payload *Payload) {
	receivedClient := &Client{}
	err := json.Unmarshal(payload.Body, receivedClient)
	if err != nil {
		h.log.Info(err.Error())
		return
	}

	session.RLock()
	client, ok := session.Clients[receivedClient.UID]
	if !ok {
		session.RUnlock()
		return
	}
	session.RUnlock()

	client.mu.Lock()
	client.IsEditor = false
	client.mu.Unlock()

	client.Send(payload)
}

func (h *Hub) handleClientLeft(session *Session, client *Client, payload *Payload) {
	h.log.Info("removing client from sessions")
	session.RemoveClient(client)
	session.Host.Send(payload)
	h.broadcastToSession(payload)
	// client.Close()
	// client = nil
}

func (h *Hub) handleClientInfo(session *Session, client *Client, payload *Payload) {
	if client.DisplayName != payload.Sender.DisplayName {
		client.mu.Lock()
		client.DisplayName = payload.Sender.DisplayName
		client.mu.Unlock()
	}
	session.Host.Send(payload)
	h.broadcastToSession(payload)
}

func (h *Hub) handleGetClients(session *Session) {
	p := &Payload{
		Type:    TypeGetClients,
		Session: session,
		Sender:  session.Host,
	}
	session.Host.Send(p)
}
