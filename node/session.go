package node

import (
	"sync"

	uuid "github.com/satori/go.uuid"
)

type Session struct {
	UID        string  `json:"uid"`
	Passphrase string  `json:"passphrase"`
	Host       *Client `json:"host"`
	Encrypted  bool    `json:"encrypted"`
	// Map of Clients in the session
	Clients map[string]*Client `json:"clients"`

	// Map of RemoteAddr hashed to string
	blockedClients map[uint64]bool
	stop           chan bool

	sync.RWMutex
}

func NewSession() *Session {
	return &Session{
		UID:            uuid.NewV4().String(),
		Passphrase:     uuid.NewV4().String(),
		Clients:        make(map[string]*Client),
		blockedClients: make(map[uint64]bool),
		stop:           make(chan bool),
	}
}

func (s *Session) RemoveClient(c *Client) {
	s.Lock()
	delete(s.Clients, c.UID)
	s.Unlock()
}

func (s *Session) Validate(payload *Payload) error {
	s.RLock()
	var client *Client
	var ok bool

	if payload.Sender.UID == s.Host.UID {
		client = s.Host
	} else if client, ok = s.Clients[payload.Sender.UID]; !ok {
		s.RUnlock()
		return ErrNonExistentClient
	}

	switch payload.Type {
	case TypeExit, TypeBlockClient, TypeUnblockClient, TypeEnforceFollow, TypeApproveWrite, TypeDenyWrite:
		if payload.Sender.UID != s.Host.UID {
			s.RUnlock()
			return ErrInvalidHostPermisssions
		}
	case TypeIncremental, TypeFullBuffer, TypeFileOpened, TypeFileClosed, TypeSelectionModified:
		if !client.IsHost || !client.IsEditor {
			s.RUnlock()
			return ErrInvalidEditorPermisssions
		}
	default:
		s.RUnlock()
		return nil
	}
	s.RUnlock()
	return nil
}
