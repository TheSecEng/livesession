package node

import (
	"errors"
	"sync"
	"time"

	"github.com/apex/log"
	"github.com/gorilla/websocket"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/theseceng/livesession/common"
	"gitlab.com/theseceng/livesession/util"
)

const (
	CloseNormalClosure     = websocket.CloseNormalClosure
	CloseInternalServerErr = websocket.CloseInternalServerErr
	CloseAbnormalClosure   = websocket.CloseAbnormalClosure
	CloseGoingAway         = websocket.CloseGoingAway
	writeWait              = 10 * time.Second
	pingInterval           = 3 * time.Second
)

var (
	expectedCloseStatuses = []int{
		websocket.CloseNormalClosure, // Reserved in case ActionCable fixes its behaviour
		websocket.CloseGoingAway,     // Web browser page was closed
		websocket.CloseNoStatusReceived,
	}
)

// Client represents active client
type Client struct {
	node        *Node
	ws          *websocket.Conn
	env         *common.SessionEnv
	send        chan Payload
	closed      bool
	connected   bool
	mu          sync.Mutex
	pingTimer   *time.Timer
	UID         string `json:"uid"`
	Session     string `json:"session"`
	DisplayName string `json:"display_name"`
	IsHost      bool   `json:"is_host"`
	IsEditor    bool   `json:"is_editor"`
	log         *log.Entry
}

type pingMessage struct {
	Type    string      `json:"type"`
	Message interface{} `json:"message"`
}

func (p *pingMessage) toJSON() []byte {
	jsonStr, err := json.Marshal(&p)
	if err != nil {
		panic("Failed to build ping JSON 😲")
	}
	return jsonStr
}

// NewHost build a new Session struct from ws connection and http request
func NewHost(node *Node, ws *websocket.Conn, url string, headers map[string]string, sessionUID string, displayName string) (*Client, error) {
	var err error
	client := &Client{
		UID:         uuid.NewV4().String(),
		Session:     sessionUID,
		DisplayName: displayName,
		IsHost:      true,
		IsEditor:    true,
		node:        node,
		ws:          ws,
		env:         common.NewSessionEnv(url, &headers),
		send:        make(chan Payload, 256),
		closed:      false,
		connected:   false,
	}
	ctx := node.log.WithFields(log.Fields{
		"sid": client.UID,
	})
	client.log = ctx

	node.hub.RLock()
	session, ok := node.hub.sessions[sessionUID]
	if !ok {
		node.hub.RUnlock()
		defer client.Close()
		return nil, errors.New("failed to create session host")
	}
	node.hub.RUnlock()

	session.Lock()
	session.Host = client
	session.Unlock()
	return client, err
}

// NewClient build a new Session struct from ws connetion and http request
func NewClient(node *Node, ws *websocket.Conn, url string, headers map[string]string, sessionUID string, passphrase string, displayName string) (*Client, error) {
	client := &Client{
		UID:         uuid.NewV4().String(),
		Session:     sessionUID,
		DisplayName: displayName,
		IsHost:      false,
		IsEditor:    false,
		node:        node,
		ws:          ws,
		env:         common.NewSessionEnv(url, &headers),
		send:        make(chan Payload, 256),
		closed:      false,
		connected:   false,
	}

	ctx := node.log.WithFields(log.Fields{
		"sid": client.UID,
	})

	client.log = ctx
	client.log.Info(client.UID)

	err := node.Authenticate(client, sessionUID, passphrase)
	if err != nil {
		defer client.Close()
		return nil, errors.New("failed to join session")
	}
	return client, err
}

func (c *Client) Start() {
	c.registerPing()
	go c.SendPayload()
	c.ReadPayloads()
}

// SendPayload sends a Payload to a client over a websocket connection
func (c *Client) SendPayload() {
	for message := range c.send {
		switch message.Type {
		case TypeCreateSuccess, TypeJoinSuccess:
			fallthrough
		case TypeClientInfo, TypeClientLeft:
			fallthrough
		case TypeFileOpened, TypeFileClosed, TypeFileFocused, TypeFullBuffer, TypeIncremental, TypeSelectionModified:
			err := c.write(message, time.Now().Add(writeWait))
			if err != nil {
				return
			}
		case TypeExit:
			err := c.write(message, time.Now().Add(writeWait))
			if err != nil {
				return
			}
			util.CloseWS(c.ws, CloseNormalClosure, "host left session")
			return
		default:
			c.log.Errorf("Unknown payload type: %v", message)
			return
		}
	}
}

// Send data to client connection
func (c *Client) Send(payload *Payload) {
	c.mu.Lock()
	if c.send == nil {
		c.mu.Unlock()
		return
	}

	select {
	case c.send <- *payload:
	default:
		if c.send != nil {
			close(c.send)
			// defer c.Disconnect()
		}
		c.send = nil
	}
	c.mu.Unlock()
}

func (c *Client) sendClose() {
	c.node.hub.RLock()
	session, ok := c.node.hub.sessions[c.Session]
	if !ok {
		c.log.Info("unable to find client session")
		c.node.hub.RUnlock()
		return
	}

	c.node.hub.RUnlock()
	payload := &Payload{
		Type:    TypeExit,
		Session: session,
		Sender:  c,
	}
	c.Send(payload)
}

func (c *Client) write(payload Payload, deadline time.Time) error {
	c.mu.Lock()
	defer c.mu.Unlock()
	data, err := json.Marshal(payload)
	if err != nil {
		return err
	}

	if err := c.ws.SetWriteDeadline(deadline); err != nil {
		return err
	}

	w, err := c.ws.NextWriter(websocket.TextMessage)

	if err != nil {
		return err
	}

	if _, err = w.Write(data); err != nil {
		return err
	}

	return w.Close()
}

func (c *Client) Sanitize(payload *Payload) (*Payload, error) {
	payload.Timestamp = time.Now()
	payload.Sender.UID = c.UID
	payload.Session.UID = c.Session
	return payload, nil
}

// ReadPayloads reads messages from ws connection and send them to node
func (c *Client) ReadPayloads() {
	for {
		var payload *Payload
		err := c.ws.ReadJSON(&payload)
		if err != nil {
			if websocket.IsCloseError(err, expectedCloseStatuses...) {
				c.log.Debugf("Websocket closed: %v", err)
				c.Disconnect()
				break
			} else if util.IsInvalidUnmarshalError(err) {
				c.log.Debugf("Websocket payload error: %v", err)
				break
			} else {
				c.log.Debugf("Websocket close error: %v %T", err, err)
				c.Disconnect()
				break
			}
		}

		payload, err = c.Sanitize(payload)
		if err != nil {
			c.log.Errorf("failed to sanitize payload: %v", err)
		}
		c.node.Broadcast(payload)
	}
}

// Disconnect enqueues closes the connection
func (c *Client) Disconnect() {
	c.node.hub.RLock()
	session, ok := c.node.hub.sessions[c.Session]
	if !ok {
		c.node.hub.RUnlock()
		c.log.Info("unable to find client session")
		c.Close()
		return
	}
	c.node.hub.RUnlock()

	c.mu.Lock()
	c.connected = false
	c.mu.Unlock()

	rawClient, err := json.Marshal(c)
	if err != nil {
		c.log.Error(err.Error())
	}

	payload := NewPayload(TypeClientLeft)
	payload.Session = session
	payload.Sender = c
	payload.Body = rawClient

	if c.IsHost {
		payload.Type = TypeExit
	}

	c.node.Broadcast(payload)
}

// Close websocket connection with the specified reason
func (c *Client) Close() {
	c.mu.Lock()
	if c.closed {
		c.mu.Unlock()
		return
	}
	c.closed = true
	c.mu.Unlock()

	c.sendClose()
	if c.pingTimer != nil {
		c.pingTimer.Stop()
	}
}

func (c *Client) ping() {
	deadline := time.Now().Add(pingInterval / 2)
	payload := NewPayload(TypePing)
	err := c.write(*payload, deadline)
	if err == nil {
		c.registerPing()
	} else {
		if c.connected {
			c.Disconnect()
		}
	}
}

func (c *Client) registerPing() {
	c.mu.Lock()
	if c.closed {
		c.mu.Unlock()
		return
	}

	c.pingTimer = time.AfterFunc(pingInterval, c.ping)
	c.mu.Unlock()
}
